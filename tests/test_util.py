from example_job_project.util import sum_ints, prefix_str

def test_sum_ints():
    assert sum_ints(1, 2) == 3

def test_prefix_str():
    assert prefix_str('my_', 'target') == 'my_target'
    assert prefix_str('your_', 'target') == 'your_target'
