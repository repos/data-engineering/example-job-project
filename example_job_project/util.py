
def sum_ints(a: int, b: int) -> int:
    """
    :return:
        a + b
    """
    return a + b

def prefix_str(prefix: str, target: str):
    """

    :param prefix:
        The prefix string

    :param target:
        The target string

    :return:
        target string prefixed with prefix string
    """
    return prefix + target
