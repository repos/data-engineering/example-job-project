# Data Engineering Example Job Project

This is a sample project that uses gitlab-ci and `conda-dist` from the
[data-engineering/workflow_utils](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils)
library to build and release a distribution conda environment.

This repository and gitlab project serve as a reference example for engineers that want to build and deploy
jobs to run in the [Analytics Data Lake](https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake),
usually scheduled using [Airflow](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Airflow).

See
https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils#building-project-conda-environments-for-distribution


# Automated releases

Releasing and versioning is handled using bump2version via workflow-utils `trigger_release` GitLab
CI Pipeline Job.  To bump the version and do a release, manually run the `trigger_release` GitLab
Job on a main branch pipeline.  This will cause the version to be bumped, and a tag and release
to be made.  Creating a tag will cause the `publish_conda_env` job to run for the tag pipeline.

If you want to control which semantic part of the version is bumped after the release, you can
change the `POST_RELEASE_VERSION_BUMP` GitLab CI variable when you run the pipeline manually.

See [workflow-utils/gitlab_ci_templates/README.md](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/blob/main/gitlab_ci_templates/README.md)
